from pydantic import BaseModel


class QuestionInput(BaseModel):
    label: str = ''
    question_body: str = ''
    active: str = ''
    datatype: str = ''

class PathInput(BaseModel):
    path: str = ''
