from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from Extractor.extractor import extractor, filewatch, database
from MedoptBackend.schemas import QuestionInput, PathInput

app = FastAPI()


origins = [
        "http://localhost:5173",
        "http://127.0.0.1:5173",
        ]

app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
        )

@app.get("/questions")
def read_questions():
    question_handler = extractor.QuestionHandler()
    questionlist = question_handler.query_all_questions()
    return questionlist

@app.post("/question")
def add_question(question: QuestionInput):
    question_handler = extractor.QuestionHandler()
    new_question = question_handler.add_question(
        label=question.label,
        question_body=question.question_body,
        datatype=question.datatype,
        active=True if question.active=='True' else False,
        )
    return new_question

@app.put("/question/{question_id}")
def update_question(question_id: int, question: QuestionInput):
    question_handler = extractor.QuestionHandler()
    question = question_handler.edit_question(
        id=question_id,
        label=question.label,
        question_body=question.question_body,
        datatype=question.datatype,
        active=True if question.active=='True' else False
        )
    return question

@app.delete("/question/{question_id}")
def delete_question(question_id: int):
    question_handler = extractor.QuestionHandler()
    question_handler.delete_question(id=question_id)
    return {
            "status": "200",
            "msg": "Question deleted successfully"
            }

@app.post("/extractor")
def run_extractor(watch_directory: PathInput):
    if not watch_directory.path[-1] == '/':
        watch_directory.path += '/'
    watch = filewatch.TheNightsWatch(watch_directory=watch_directory.path, watch_interval=5)
    watch.inital_files()

    extract = extractor.extractor(watch_directory=watch_directory.path)

    extract.process_files()

    return {
            "status": "200",
            "msg": "Extractor ran successfully"
            }

@app.post("/extractor/reset")
def reset_extractor():
    database_handler = database.DatabaseHandler()

    database_handler.reset_processed_files()

    return {
            "status": "200",
            "msg": "Extractor ran successfully"
            }
