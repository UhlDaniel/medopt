""" This file contains the converter functions for the extractor model. """
import re


def convert_nothing(answer):
    """Returns the answer as is.

    Args:
        answer (str): The answer to the question.

    Returns:
        str: The answer to the question.
    """
    return answer['answer']

def convert_int(answer):
    """Converts the answer to an integer.

    Args:
        answer (str): The answer to the question.

    Returns:
        int: The answer to the question.
    """
    result = re.findall(r'\b\d+\b', answer['answer'])
    if result:
        return int(result[0])
    return None

def convert_float(answer):
    """Converts the answer to a float.

    Args:
        answer (str): The answer to the question.

    Returns:
        float: The answer to the question.
    """
    result = re.findall(r'\b[+-]?[0-9]+\.[0-9]+\b', answer['answer'])
    if result:
        return float(result[0])
    return None

def convert_bool(answer):
    """Converts the answer to a boolean.

    Args:
        answer (str): The answer to the question.

    Returns:
        bool: The answer to the question.
    """
    return True if answer['answer'].lower() == "true" else False
