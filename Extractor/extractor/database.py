from datetime import datetime
from .models import File, create_diagnosis_table
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


# Database used for question storage and file logging.
QUESTION_DB_URL = "sqlite:///./medopt-db.sql"

question_engine = create_engine(QUESTION_DB_URL, echo=True)
QuestionDBSession = sessionmaker(question_engine, autoflush=False)


class AnswerDatabase:
    """Database used for answer storage."""
    def __init__(self, database_name):
        """Initializes the AnswerDatabase class.
        
        Args:
            database_url (str): The url of the database.
        """
        now = datetime.now().strftime("%Y-%m-%d-%H%M%S")
        self.engine = create_engine(f"sqlite:///./{database_name}_{now}.sql", echo=True)
        self.DBSession = sessionmaker(self.engine, autoflush=False)

class DatabaseHandler:
    """Handles answer database interactions."""
    def __init__(self):
        """Initializes the DatabaseHandler class."""
        self.answer_db = None
        self.question_db = None
        self.Base = None
        self.Diagnosis = None

    def create_answer_db(self, database_name):
        """Creates a new answer database.
        
        Args:
            database_name (str): The name of the database.
        """
        self.answer_db = AnswerDatabase(database_name)
    
    def reset_answer_db(self, database_name):
        """Resets the answer database.
        
        Args:
            database_name (str): The name of the database.
        """
        self.answer_db = AnswerDatabase(database_name)
        self.reset_processed_files()
    
    def reset_processed_files(self):
        """Resets the processed status of all files in the question database."""
        self.question_db = QuestionDBSession()
        try:
            for file in self.question_db.query(File).all():
                file.processed = False
            self.question_db.commit()
        finally:
            self.question_db.close()
    
    def create_table(self, label, datatype):
        """Creates a table in the answer database.
        
        Args:
            label (str): The label of the question.
            datatype (str): The datatype of the answer.
        """
        Diagnosis, Base = create_diagnosis_table(label, datatype)
        Base.metadata.create_all(bind=self.answer_db.engine)

    def add_entry(self, label, datatype, answer):
        """Adds an entry to the answer database.
        
        Args:
            label (str): The label of the question.
            answer (dict): The answer to the question.
        """
        db = self.answer_db.DBSession()
        Diagnosis, Base = create_diagnosis_table(label, datatype)
        try:
            diagnosis = Diagnosis()
            for key, value in answer.items():
                setattr(diagnosis, key, value)
            db.add(diagnosis)
            db.commit()
            db.refresh(diagnosis)
        finally:
            db.close()
        
