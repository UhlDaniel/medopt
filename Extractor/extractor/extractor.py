from transformers import pipeline
from .models import File, Question, create_diagnosis_table
from .database import QuestionDBSession, DatabaseHandler
from .converters import convert_int, convert_nothing, convert_float, convert_bool
from sqlalchemy import Integer, String, Boolean, Float


# Supported datatypes
DATATYPES = {"Integer": Integer,
             "String": String,
             "Boolean": Boolean,
             "Float": Float}

# Included converters
CONVERTER = {"Integer": convert_int,
             "String": convert_nothing,
             "Boolean": convert_bool,
             "Float": convert_float}


class extractor:
    """ This class contains the extractor model. """

    def __init__(self, watch_directory="testfiles/"):
        """Initializes the extractor model.

        Args:
            watch_directory (str): The directory to watch for new files.
        """
        self.watch_directory = watch_directory
        self.context = """Extractive Question Answering is the task of extracting
            an answer from a text given a question. An example of a question answering
            dataset is the SQuAD dataset, which is entirely based on that task. If you
            would like to fine-tune a model on a SQuAD task, you may leverage the
            examples/question-answering/run_squad.py script."""
        self.QuestionHandler = QuestionHandler()
        self.DatabaseHandler = DatabaseHandler()
        self.filenames = []
        self.questions = []
        self.client_id = None
        self.question_answerer = pipeline("question-answering",
                                    model='distilbert-base-uncased-distilled-squad')

        # self.question_answerer = pipeline("question-answering",
        #                             model='deepset/roberta-base-squad2')

    def answer(self, question):
        """Answers a question based on the context.

        Args:
            question (str): The question to answer.
        """
        return self.question_answerer(question=question, context=self.context)

    def query_unprocessed_filenames(self):
        """Queries the database for all unprocessed filenames."""
        db = QuestionDBSession()
        try:
            files = db.query(File).filter(File.processed == False).all()
        finally:
            db.close()
        self.filenames = [f.filename for f in files]
    
    def mark_file_processed(self, filename):
        """Marks a file as processed in the database.

        Args:
            filename (str): The filename to mark as processed.
        """
        db = QuestionDBSession()
        try:
            active_file = db.query(File).filter(File.filename == filename).first()
            active_file.processed = True
            db.commit()
            db.refresh(active_file)
        finally:
            db.close()

    def read_file(self, filename):
        """Loads a file into the context.

        Args:
            filename (str): The filename to load.
        """
        with open(self.watch_directory + filename, 'r') as f:
            content = f.read()
        content = content.split('\n')
        self.client_id = int(content[0][4:])
        self.context = content[2]

    def process_file(self, filename):
        """Processes a file and adds the answers to the database.

        Args:
            filename (str): The filename to process.
        """
        self.read_file(filename)
        for question in self.questions:
            answer = self.answer(question.question_body)
            print(answer)
            new_diagnosis = {"client_id": self.client_id,
                             "filename": filename,
                             "value": CONVERTER[question.datatype](answer),
                             "raw_value": answer["answer"],
                             "raw_score": answer["score"],
                             "raw_start": answer["start"],
                             "raw_end": answer["end"]}
 
            db = QuestionDBSession()
            print(new_diagnosis)
            self.DatabaseHandler.add_entry(question.label, DATATYPES[question.datatype], new_diagnosis)
        self.mark_file_processed(filename)

    def process_files(self):
        """Processes all files in the filenames list."""
        # get active questions
        self.DatabaseHandler.reset_answer_db("medopt-db-answer")
        self.questions = self.QuestionHandler.query_active_questions()
        for question in self.questions:
            self.DatabaseHandler.create_table(question.label, DATATYPES[question.datatype])
        # get filenames unprocessed files
        self.query_unprocessed_filenames()
        print("Processing files:")
        print(self.filenames)
        for filename in self.filenames:
            self.process_file(filename)


class QuestionHandler:
    """ This class handles the questions for the extractor. """

    def add_question(self, label, question_body, datatype, active):
        """Adds a question to the database.

        Args:
            label (str): The label of the question.
            question (str): The question.
            datatype_body (str): The datatype of the answer.
            active (bool): The active status of the question.
        """
        db = QuestionDBSession()
        try:
            new_question = Question(
                    label=label,
                    question_body=question_body,
                    datatype=datatype,
                    active=active
                    )
            db.add(new_question)
            db.commit()
            db.refresh(new_question)
        finally:
            db.close()
        return new_question

    def delete_question(self, id):
        """Removes a question from the database.

        Args:
            id (int): The id of the question.
        """
        db = QuestionDBSession()
        try:
            db.query(Question).filter(Question.id == id).delete()
            db.commit()
        finally:
            db.close()

    def edit_question(self, id, label, question_body, datatype, active):
        """Edits a question in the database.

        Args:
            id (int): The id of the question.
            label (str): The label of the question.
            question_body (str): The question.
            datatype (str): The datatype of the answer.
            active (bool): The active status of the question.
        """
        db = QuestionDBSession()
        try:
            question = db.query(Question).filter(Question.id == id).first()
            question.label = label
            question.question_body = question_body
            question.datatype = datatype
            question.active = active
            db.commit()
            db.refresh(question)
        finally:
            db.close()
        return question

    def toggle_question(self, id):
        """Toggles the active status of a question in the database.

        Args:
            id (int): The id of the question.
        """
        db = QuestionDBSession()
        try:
            question = db.query(Question).filter(Question.id == id).first()
            question.active = not question.active
            db.commit()
            db.refresh(question)
        finally:
            db.close()

    def query_active_questions(self):
        """Queries the database for all questions."""
        db = QuestionDBSession()
        try:
            questions = db.query(Question).filter(Question.active == True).all()
        finally:
            db.close()
        return questions

    def query_all_questions(self):
        """Queries the database for all questions."""
        db = QuestionDBSession()
        try:
            questions = db.query(Question).all()
        finally:
            db.close()
        return questions
