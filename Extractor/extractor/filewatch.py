"""
This module contains tools for the extractor model.
"""

from .database import QuestionDBSession
from .models import File
import pathlib
import time
import watchdog.events
import watchdog.observers


class TheNightsWatch:
    """Logs all the events captured by the watchdog observer.

    Args:
        watch_interval (int): The interval in seconds to check for new files.
        watch_directory (str): The directory to watch for new files.
    """

    def __init__(self, watch_directory="testfiles/", watch_interval=5):
        """Initializes the TheNightsWatch class.

        Args:
            watch_interval (int): The interval in seconds to check for new files.
            watch_directory (str): The directory to watch for new files.
        """
        self.watch_interval = watch_interval
        self.watch_directory = watch_directory
        self.observer = watchdog.observers.Observer()

    def run(self):
        """Runs the observer."""
        self.inital_files()
        event_handler = WatchHandler()
        self.observer.schedule(event_handler, self.watch_directory, recursive = True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
        self.observer.join()

    def inital_files(self):
        """Adds all the files in the watch directory to the database at startup."""
        files = [f.name for f in pathlib.Path(self.watch_directory).glob("*.txt")]
        db = QuestionDBSession()
        try:
            db_files = db.query(File).all()
        finally:
            db.close()

        files = [f for f in files if f not in [dbf.filename for dbf in db_files]]

        if files:
            db = QuestionDBSession()
            try:
                for file in files:
                    new_file = File(
                            filename=file,
                            processed=False
                            )
                    db.add(new_file)
                    db.commit()
                    db.refresh(new_file)
            finally:
                db.close()


class WatchHandler(watchdog.events.PatternMatchingEventHandler):
    """Watchdog event handler."""
    def __init__(self):
        """Initializes the WatchHandler class."""
        # Set the patterns for PatternMatchingEventHandler
        watchdog.events.PatternMatchingEventHandler.__init__(self, patterns=['*.csv'],
                                                             ignore_directories=True,
                                                             case_sensitive=False)

    def on_created(self, event: watchdog.events.FileCreatedEvent):
        """Adds a file to the database when it is created.
        
        Args:
            event (watchdog.events.FileCreatedEvent): The event that triggered the handler.
        """
        db = QuestionDBSession()
        try:
            new_file = File(
                    filename=event.src_path.split("/")[-1],
                    processed=False
                    )
            db.add(new_file)
            db.commit()
            db.refresh(new_file)
        finally:
            db.close()
