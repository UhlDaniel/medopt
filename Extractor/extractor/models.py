from sqlalchemy import Column, Integer, String, Boolean, Float
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class File(Base):

    __tablename__ = "files"

    id = Column(Integer, primary_key=True)
    filename = Column(String, nullable=False, unique=True)
    processed = Column(Boolean, default=False)

    def __repr__(self):
        return f'File(id={self.id}, filename={self.filename}, '\
               f'processed={self.processed})'


class Client(Base):

    __tablename__ = "client"

    id = Column(Integer, primary_key=True)
    client_id = Column(Integer)
    filename = Column(String)

    def __repr__(self):
        return f'Client(id={self.id}, client_id={self.client_id}, '\
               f'filename={self.filename})'


class Question(Base):

    __tablename__ = "questions"

    id = Column(Integer, primary_key=True)
    label = Column(String)
    question_body = Column(String)
    active = Column(Boolean, default=True)
    datatype = Column(String)

    def __repr__(self):
        return f'Questions(id={self.id}, label={self.label}, '\
               f'question={self.question_body}, active={self.active}, '\
               f'datatype={self.datatype})'


def create_diagnosis_table(label, datatype):
    """Creates a table for a diagnosis.
    
    Args:
        label (str): The label of the diagnosis.
        datatype (str): The datatype of the diagnosis.
    """
    CustomBase = declarative_base()
    class Diagnosis(CustomBase):

        __tablename__ = label

        id = Column(Integer, primary_key=True)
        client_id = Column(Integer)
        filename = Column(String)
        value = Column(datatype)
        raw_value = Column(String)
        raw_start = Column(Integer)
        raw_end = Column(Integer)
        raw_score = Column(Float)

        def __repr__(self):
            return f'Diagnosis(id={self.id}, client_id={self.client_id}, '\
                   f'filename={self.filename}, value={self.value}), '\
                   f'raw_value={self.raw_value}, raw_start={self.raw_start}, '\
                   f'raw_end={self.raw_end}, raw_score={self.raw_score})'

    return Diagnosis, CustomBase   
