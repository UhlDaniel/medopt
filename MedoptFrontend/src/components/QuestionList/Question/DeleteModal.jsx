import { useContext } from "react";
import axios from "axios";

import { QuestionsListUpdateFunctionContext } from "../../../App";
import "./DeleteModal.styles.css";

export default function DeleteModal({ questionId, showDeleteModal }) {
  const setQuestions = useContext(QuestionsListUpdateFunctionContext);
  const handleYesClick = async () => {
    const API_URL = "http://localhost:8000";
    await axios.delete(`${API_URL}/question/${questionId}`);
    const { data } = await axios.get(`${API_URL}/questions`);
    setQuestions(data);
    showDeleteModal(false);
  };
  const handleNoClick = () => {
    showDeleteModal(false);
  };
  return (
    <div id="delete-modal-container">
      <div id="delete-modal">
        <p id="prompt-msg">Delete this Question?</p>
        <div id="btn-container">
          <button id="yes-btn" onClick={() => handleYesClick()}>
            Yes
          </button>
          <button id="no-btn" onClick={handleNoClick}>
            No
          </button>
        </div>
      </div>
    </div>
  );
}