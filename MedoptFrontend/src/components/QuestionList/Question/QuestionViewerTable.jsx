import { useState } from "react";
import DeleteModal from "./DeleteModal";

import "./Question.styles.css";

function activeQuestion(active) {
  if (active) {
    return "✓";
  } else {
    return "✖";
  }
}

export default function QuestionViewer({ question, setQuestionView }) {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const { id, label, question_body, active, datatype } = question;
  const handleDeleteQuestion = () => {
    setShowDeleteModal(true);
  };
  return (
    <div>
      {showDeleteModal && (
        <DeleteModal showDeleteModal={setShowDeleteModal} questionId={id} />
      )}
      <tr>
      <td>{label}</td>
      <td>{question_body}</td>
      <td>{activeQuestion(active)}</td>
      <td>{datatype}</td>
      <td>
        <button
          className="neutral-btn"
          onClick={() => setQuestionView("editing")}
        >
          Edit
        </button>
      </td>
      <td>
        <button
          className="delete-btn"
          onClick={() => handleDeleteQuestion()}
        >
          Delete
        </button>
      </td>
      </tr>
    </div>
  );
}
