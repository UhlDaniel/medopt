import axios from "axios";
import { useState, useContext } from "react";
import { QuestionsListUpdateFunctionContext } from "../../../App";


import "./Question.styles.css";

export default function QuestionEditor({ question, setQuestionView }) {
  const [questionBody, setQuestionBody] = useState(question.question_body);
  const [questionActive, setQuestionActive] = useState(question.active);
  const [questionDatatype, setQuestionDatatype] = useState(question.datatype);
  const [isInvalidSave, setIsInvalidSave] = useState(false);
  const setQuestions = useContext(QuestionsListUpdateFunctionContext);
  const handleQuestionSave = async (event, id) => {
    event.preventDefault();
    if (questionBody.length > 0 && questionDatatype.length > 0) {
      const API_URL = "http://localhost:8000";
      await axios.put(`${API_URL}/question/${id}`, {
        label: question.label,
        question_body: questionBody,
        active: questionActive,
        datatype: questionDatatype,
      });
      const { data } = await axios.get(`${API_URL}/questions`);
      setQuestions(data);
      setQuestionView("viewing");
    } else {
      setIsInvalidSave(true);
      console.log(questionDatatype > 0);
      // questionLabelInputRef.current.focus();
    }
  };
  return (
    <form
      id="question-container"
      onSubmit={(event) => handleQuestionSave(event, question.id)}
    >
      <div className="question-label">{question.label}</div>
      <input
        type="text"
        placeholder="Enter Question"
        id="question-body-edit-input"
        className={isInvalidSave ? "input-error" : ""}
        value={questionBody}
        onChange={(event) => {
          setIsInvalidSave(false);
          setQuestionBody(event.target.value);
        }}
      />
      <input
        type="checkbox"
        id="question-active-edit-input"
        className={isInvalidSave ? "input-error" : ""}
        checked={questionActive}
        onChange={(event) => {
          setIsInvalidSave(false);
          setQuestionActive(!questionActive);
        }}
      />
      <input
        type="text"
        id="question-datatype-edit-input"
        className={isInvalidSave ? "input-error" : ""}
        value={questionDatatype}
        onChange={(event) => {
          setIsInvalidSave(false);
          setQuestionDatatype(event.target.value);
        }}
      />
      <div className="question-buttons-container">
        <button className="save-btn" type="submit">
          Save
        </button>
        <button
          className="neutral-btn"
          type="button"
          onClick={() => setQuestionView("viewing")}
        >
          Cancel
        </button>
      </div>
    </form>
  );
}
