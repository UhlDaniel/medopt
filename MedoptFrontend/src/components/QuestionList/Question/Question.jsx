import { useState } from "react";

import QuestionViewer from "./QuestionViewer";
import QuestionEditor from "./QuestionEditor";


export default function Question({ question }) {
  const [questionView, setQuestionView] = useState("viewing");
  switch (questionView) {
    case "viewing":
      return <QuestionViewer question={question} setQuestionView={setQuestionView} />;
    case "editing":
      return <QuestionEditor question={question} setQuestionView={setQuestionView} />;
    default:
      return <></>;
  }
}
