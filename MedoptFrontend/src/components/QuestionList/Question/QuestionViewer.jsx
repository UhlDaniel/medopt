import { useState } from "react";
import DeleteModal from "./DeleteModal";

import "./Question.styles.css";

function activeQuestion(active) {
  if (active) {
    return "✓";
  } else {
    return "✖";
  }
}

export default function QuestionViewer({ question, setQuestionView }) {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const { id, label, question_body, active, datatype } = question;
  const handleDeleteQuestion = () => {
    setShowDeleteModal(true);
  };
  return (
    <div id="question-container">
      {showDeleteModal && (
        <DeleteModal showDeleteModal={setShowDeleteModal} questionId={id} />
      )}
      <div className="question-label">{label}</div>
      <div className="question-body">{question_body}</div>
      <div className="question-active">{activeQuestion(active)}</div>
      <div className="question-datatype">{datatype}</div>
      <div className="question-buttons-container">
        <button
          className="neutral-btn"
          onClick={() => setQuestionView("editing")}
        >
          Edit
        </button>
        <button
          className="delete-btn"
          onClick={() => handleDeleteQuestion()}
        >
          Delete
        </button>
      </div>
    </div>
  );
}
