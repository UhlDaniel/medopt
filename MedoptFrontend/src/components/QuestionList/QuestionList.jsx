import Question from "./Question/Question";

import "./QuestionList.styles.css";


export default function QuestionList({ questions}) {
  return (
    <div id="questions-list-container">
      <h2 id="questions-list-header">Questions</h2>
      <ul id="questions-list">
        {questions.map((question) => (
          <li key={question.id}>
            <Question question={question} />
          </li>
        ))}
      </ul>
    </div>
  );
}