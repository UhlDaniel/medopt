import { useState, useContext } from "react";
import axios from "axios";

import { QuestionsListUpdateFunctionContext } from "../../App";
import "./AddQuestion.styles.css";


export default function AddQuestion() {
  const [questionLabel, setQuestionLabel] = useState("");
  const [questionBody, setQuestionBody] = useState("");
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);
  const [hasInputError, setHasInputError] = useState(false);
  const [questionActive, setQuestionActive] = useState(true);
  const [questionDatatype, setQuestionDatatype] = useState("Integer");
  const setQuestions = useContext(QuestionsListUpdateFunctionContext);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (questionLabel.length > 0 && questionBody.length > 0) {
      setIsFormSubmitting(true);
      const API_URL = "http://localhost:8000";
      const { data } = await axios.post(`${API_URL}/question`, {
        label: questionLabel,
        question_body: questionBody,
        active: questionActive,
        datatype: questionDatatype,
      });
      setQuestions((prev) => [...prev, data]);
    } else {
      setHasInputError(true);
    }
    setQuestionLabel("");
    setQuestionBody("");
    setIsFormSubmitting(false);
  };
  return (
    <div>
    <h2 id="add-question-header">Add Question</h2>
    <form onSubmit={(event) => handleSubmit(event)} id="add-question-form">
      <input
        type="text"
        placeholder="Enter Label"
        id="question-label-input"
        className={hasInputError ? "input-error" : ""}
        value={questionLabel}
        onChange={(event) => {
          setHasInputError(false);
          setQuestionLabel(event.target.value);
        }}
      />
      <input
        type="text"
        placeholder="Enter Question"
        id="question-body-input"
        className={hasInputError ? "input-error" : ""}
        value={questionBody}
        onChange={(event) => {
          setHasInputError(false);
          setQuestionBody(event.target.value);
        }}
      />
      <input
        type="checkbox"
        id="question-active-input"
        checked={questionActive}
        onChange={(event) => {
        //   setIsInvalidSave(false);
          setQuestionActive(!questionActive);
        }}
      />
      <input
        type="text"
        placeholder="Enter Datatype"
        id="question-datatype-input"
        value={questionDatatype}
        onChange={(event) => {
          // setIsInvalidSave(false);
          setQuestionDatatype(event.target.value);
        }}
      />
      <button id="add-question-btn" type="submit" disabled={isFormSubmitting}>
        {isFormSubmitting ? "..." : "Add Question"}
      </button>
    </form>
    </div>
  );
}