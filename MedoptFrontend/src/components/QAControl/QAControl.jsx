import { useState } from "react";
import axios from "axios";

import "./QAControl.styles.css";


export default function QAControl() {
  const [filePath, setFilePath] = useState("");
  const [isResetSubmitting, setIsResetSubmitting] = useState(false);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);
  const [hasInputError, setHasInputError] = useState(false);

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    if (filePath.length > 0) {
      setIsFormSubmitting(true);
      const API_URL = "http://localhost:8000";
      const { data } = await axios.post(`${API_URL}/extractor`, {
        path: filePath,
      });
    } else {
      setHasInputError(true);
    }
    setIsFormSubmitting(false);
  };

  const handleResetSubmit = async (event) => {
    event.preventDefault();
    if (filePath.length > 0) {
      setIsResetSubmitting(true);
      const API_URL = "http://localhost:8000";
      const { data } = await axios.post(`${API_URL}/extractor/reset`, {});
    } else {
      setHasInputError(true);
    }
    setIsResetSubmitting(false);
  };
  return (
    <div>
    <h2 id="qa-control-header">QA Control</h2>
    <form onSubmit={(event) => handleFormSubmit(event)} id="qa-control-form">
      <input
        type="text"
        placeholder="Enter relative path"
        id="qa-control-filepath-input"
        className={hasInputError ? "input-error" : ""}
        value={filePath}
        onChange={(event) => {
          setHasInputError(false);
          setFilePath(event.target.value);
        }}
      />
      <button id="qa-control-btn" type="submit" disabled={isFormSubmitting}>
        {isFormSubmitting ? "..." : "Run Extractor"}
      </button>
      <button id="qa-reset-btn" disabled={isResetSubmitting} onClick={handleResetSubmit}>
        {isResetSubmitting ? "..." : "Reset Files"}
      </button>
    </form>
    </div>
  );
}
  