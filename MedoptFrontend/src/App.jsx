import { useState, useEffect, createContext } from "react";
import axios from "axios";

import AddQuestion from "./components/AddQuestion/AddQuestion";
import QuestionList from "./components/QuestionList";
import QAControl from "./components/QAControl/QAControl";

import "./App.css";


export const QuestionsListUpdateFunctionContext = createContext(null);

export default function App() {
  const [questions, setQuestions] = useState([]);
  useEffect(() => {
    console.log("useEffect");
    const getQuestions = async () => {
      console.log("getQuestions");
      const API_URL = "http://localhost:8000";
      const { data } = await axios.get(`${API_URL}/questions`);
      setQuestions(data);
    };
    getQuestions();
  }, []);
  return (
    <QuestionsListUpdateFunctionContext.Provider value={setQuestions}>
      <div>
        <h1 id="app-title">MedOpt SQUAD model helper</h1>
        <QAControl />
        <AddQuestion />
        <QuestionList questions={questions} />
      </div>
    </QuestionsListUpdateFunctionContext.Provider>
  );
}