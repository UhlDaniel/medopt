# MedOps Extracter

This repository contains a question answering model designed to extract data from clinical reports.

## Usage

To use the MedOps (in development mode), follow these steps:

1. Clone the repository:

    ```bash
    git clone https://github.com/your-username/medops.git
    cd medops
    ```

2. Run the *fastapi* backend:

    ```bash
    python run.py
    ```

3. Run the *reactjs* frontend using npm:

    ```bash
    npm run dev
    ```

4. Open a Browser and connect to http://localhost:5173/


## Additional information

The folder *./testfiles* contains two simplified medical report to demonstrate the functionallity.

The first line of the report must consist of the user ID followed by an empty line. See the example report below.

```
ID: 12345678

Content of the medical report.
```



## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more information.
